package com.example.mati.gallery_app;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

/**
 * Created by Mati on 05.06.2016.
 */

public class MainActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private ImageAdapter imageAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setImageGrid();
        new GetImageListTask().execute(imageAdapter);
    }

    private void setImageGrid() {
        mRecyclerView = (RecyclerView) findViewById(R.id.rv_image_list);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new GridLayoutManager(MainActivity.this, 3);
        mRecyclerView.setLayoutManager(mLayoutManager);

        imageAdapter = new ImageAdapter(getApplicationContext());
        mRecyclerView.setAdapter(imageAdapter);
    }
}