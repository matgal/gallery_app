package com.example.mati.gallery_app;

import android.os.AsyncTask;
import android.os.Environment;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mati on 05.06.2016.
 */

public class GetImageListTask extends AsyncTask<ImageAdapter, Void, ArrayList<String>> {

    ArrayList<String> inFiles;
    private ImageAdapter imageAdapter;

    public List<String> getFilesList() {
        inFiles = new ArrayList<>();
        String picturesDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath();
        File directory = new File(picturesDir);
        travDirectory(directory);
        return inFiles;
    }

    public void travDirectory(File root) {
        File[] dirList = root.listFiles();
        for (File file : dirList) {
            if (file.isDirectory()) {
                travDirectory(file);
            }
            else {
                inFiles.add(String.valueOf(file.getAbsoluteFile()));
            }
        }
    }

    @Override
    protected ArrayList<String> doInBackground(ImageAdapter... params) {
        imageAdapter = params[0];
        return (ArrayList<String>) getFilesList();
    }

    @Override
    protected void onPostExecute(ArrayList<String> s) {
        super.onPostExecute(s);
        imageAdapter.setPaths(s);
    }
}
