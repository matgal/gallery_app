package com.example.mati.gallery_app;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.bumptech.glide.Glide;

/**
 * Created by Mati on 06.06.2016.
 */

public class ImageFullscreen extends AppCompatActivity {

    public String path;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(R.anim.animation_in, R.anim.animation_out);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_image_fullscreen);
        TouchImageView imageView = new TouchImageView(this);
        Bundle bundle = getIntent().getExtras();
        path = bundle.getString("uri");

        Glide.with(this)
                .load(path)
                .into(imageView);

        imageView.setMaxZoom(4f);
        setContentView(imageView);
    }
}