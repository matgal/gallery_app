package com.example.mati.gallery_app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

/**
 * Created by Mati on 05.06.2016.
 */

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ViewHolder> {

    public ArrayList<String> paths;

    public ImageAdapter(Context applicationContext) {
        paths = new ArrayList<>();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView thumbnail;
        public Context context;
        public String path;

        public ViewHolder(View view) {
            super(view);
            itemView.setOnClickListener(this);
            context = itemView.getContext();
            thumbnail = (ImageView) view.findViewById(R.id.image_item);
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(context, ImageFullscreen.class);
            Bundle bundle = new Bundle();
            bundle.putString("uri", path);
            intent.putExtras(bundle);
            context.startActivity(intent);
        }
    }

    public void setPaths(ArrayList<String> paths) {
        this.paths.clear();
        this.paths.addAll(paths);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.image_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ImageAdapter.ViewHolder holder, int position) {
        holder.path = paths.get(position);

        Glide.with(holder.context)
                .load(paths.get(position))
                .thumbnail(0.5f)
                .centerCrop()
                .into(holder.thumbnail);
    }

    @Override
    public int getItemCount() {
        return paths.size();
    }
}
